bach is a independent project by composers [Andrea Agostini](http://brahms.ircam.fr/andrea-agostini) and [Daniele Ghisi](http://www.danieleghisi.com). For this reason its main focus is on musical representation, analysis and composition. 
bach is licensed under the Creative Common BY-NC-SA 3.0 and is free for non-commercial use.

![logo-bach](https://forum.ircam.fr/media/uploads/software/bach%20computer%20aided%20compistion%20in%20Max/bach_logo_complete_centered_pad.png) 

## What bach brings ##

- Music Notation: bach brings music notation inside [Max](https://forum.ircam.fr/projects/detail/max-8/). You can generate, edit, script, modify and play your scores either with mouse and keyboard or through patching. Any Max audio module can be easily driven.

- Reactive Paradigm: bach is a true citizen of Max, perfectly incarnating its real-time paradigm. Interact with scores in a reactive way, as you would with sounds or images! From simple recording to complex processing, a new world of symbolic treatments is at the end of your fingertips.

![](https://forum.ircam.fr/media/uploads/software/bach%20computer%20aided%20compistion%20in%20Max/rt_generation.png)![](https://forum.ircam.fr/media/uploads/software/bach%20computer%20aided%20compistion%20in%20Max/offline_cac.png)

## What you can do with bach ##

- [Composition](https://www.bachproject.net/tag/composition/): Compose your music using bach: write, generate or modify your scores intuitively and interactively.

- [Live notation](https://www.bachproject.net/tag/live-notation/): Create and display music on the fly for your performers, or for any other usage.

- [Education](https://www.bachproject.net/tag/education/): Build intuitive and interactive tools for music education. Tailor the amount of allowed interaction to your specific needs, have your students play with your patch and then automatically evaluate their results.

- [augmented sequencers](https://www.bachproject.net/tag/augmented-sequencers/) : Use bach.roll and bach.score as symbolic sequencers, triggering soundfiles or DSP processes; since each note carries flexible meta-information, organized in slots, you can control and customize any meaningful sequencing parameter.

- [synthesis](https://www.bachproject.net/tag/synthesis/): Use bach.roll and bach.score to control synthesis modules inside Max; put any meaningful parameter (numbers, envelopes, instructions...) inside the note slots, and retrieve them at playtime.

- [video and lighting](https://www.bachproject.net/tag/multimedia/): Sequence or generate video content in Jitter; use scores to control and automate any external Max-compatible device, such as DMX lighting scenarios or physical actuators.

- [constraint programming](https://www.bachproject.net/tag/constraint-programming/): Generate data by defining the properties they should match, without writing a single line of code.

- Analysis: Segment and analyze scores in real-time or off-line. Organize your scores in a database, and display it.

- [metascores]( https://www.bachproject.net/tag/metascores/): Build scores where each note is a complex process in itself; display process information dynamically and render audio or symbolic result properly (both at playtime and off-line).

## Features ##

- Runs on Windows or OSX.

- Even if you aren't a fan of music notation, bach might still help you by adding two new data types to the Max environment: rational numbers and lllls (Lisp-like linked lists). The latter can contain other lllls, a bit like a Lisp list. Unlike Max lists, bach lllls have no limit in size, an essential feature to deal with long collections of data.

- [Public API](https://www.bachproject.net/dl/).

- bach implements both classical music notation and proportional music notation, with support for accidentals of arbitrary resolution, rhythmic trees and grace notes, polymetric notation, MusicXML, MIDI files and much more. The graphic position of all notation elements can be queried so that reactive customized notation systems can be built.

## Libraries currently in the bach family ##

![logo-cage](https://forum.ircam.fr/media/uploads/software/bach%20computer%20aided%20compistion%20in%20Max/cage_logo_web_v.png) ![logo-dada](https://forum.ircam.fr/media/uploads/software/bach%20computer%20aided%20compistion%20in%20Max/dada_logo_web_v-1.png)

bach is the head of a family of libraries which share their core philosophy and basic working principles. Each library in the growing bach family focuses on some particular aspects of reactive symbolic composition: [cage](https://www.bachproject.net/cage/) and [dada](https://www.bachproject.net/dada/) are already available.

> - [Official site](https://www.bachproject.net)‘s
> - [Free Download](https://www.bachproject.net/dl/)
> - Contact: info(at)bachproject.net
> - [Forum](https://www.bachproject.net/forum/)

> **Bit-makers with**
> - [Andrea Agostini](https://forum.ircam.fr/article/detail/bit-markers-avec-andrea-agostini/)
> - [Daniele Ghisi](https://forum.ircam.fr/article/detail/bit-makers-rencontre-avec-daniele-ghisi/)

>
> **Related**
> - [bach with Antescofo](https://www.bachproject.net/2016/10/07/bach-with-antescofo/)
> - [Orchids](https://forum.ircam.fr/projects/detail/orchids/): bach, cage and dada are throroughly used in Ircam’s Orchids project, as interface tools for the automatic orchestration engine.

